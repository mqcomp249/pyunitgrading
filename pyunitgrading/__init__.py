# -*- coding: utf-8 -*-

__author__ = 'Steve Cassidy'
__email__ = 'Steve.Cassidy@mq.edu.au'
__version__ = '0.1.0'

from pyunitgrading.filehandling import unpack_submissions
from pyunitgrading.testrunner import process